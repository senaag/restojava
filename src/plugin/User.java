/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plugin;

/**
 *
 * @author sena
 */
public class User {
    //buat variabel berdasarkan kolom di tabel user
    private static String nama;
    private static String nama_lengkap;
    private static String pasword;
    private static String keterangan;

    public static String getNama() {
        return nama;
    }

    public static void setNama(String nama) {
        User.nama = nama;
    }

    public static String getNama_lengkap() {
        return nama_lengkap;
    }

    public static void setNama_lengkap(String nama_lengkap) {
        User.nama_lengkap = nama_lengkap;
    }

    public static String getPasword() {
        return pasword;
    }

    public static void setPasword(String pasword) {
        User.pasword = pasword;
    }

    public static String getKeterangan() {
        return keterangan;
    }

    public static void setKeterangan(String keterangan) {
        User.keterangan = keterangan;
    }
    
    //metode untuk menghapus data
    public static void hapusData(){
        nama = "";
        nama_lengkap = "";
        pasword = "";
        keterangan = "";
    }
    
    
}
